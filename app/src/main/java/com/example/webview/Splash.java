package com.example.webview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class Splash extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                startActivity(new Intent(getApplicationContext(), DrawerMainActivity.class));
                Bundle b = new Bundle();
                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                b.putString("urls","https://moneychanger.co.id/code/demo.php");
                i.putExtras(b);
                startActivity(i);
                finish();
            }
        },3000);
    }
}
